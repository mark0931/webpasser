/*
 * 系统名称: 
 * 模块名称: webpasser.core
 * 类 名 称: CustomConfig.java
 *   
 */
package com.hxt.webpasser.transport.xml;

import java.util.List;

/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 作者: hanxuetong <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CustomConfig {

	private List<CustomRule> customRules;

	public List<CustomRule> getCustomRules() {
		return customRules;
	}

	public void setCustomRules(List<CustomRule> customRules) {
		this.customRules = customRules;
	}
	
	
}
